import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoDashboardComponent } from './video-dashboard/video-dashboard.component';
import { VideoListComponent } from './video-list/video-list.component';

@NgModule({
  declarations: [VideoDashboardComponent, VideoListComponent],
  imports: [
    CommonModule
  ]
})
export class DashboardModule { }
